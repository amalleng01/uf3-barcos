package net.xeill.elpuig;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;


/**
* This class implements the Messages
*
* @author ASIX1
*
*/

public class Messages {
  Scanner input = new Scanner(System.in);

  Colores colores = new Colores();



  public void mainMenuVenta() {
    System.out.println(colores.GREEN_BOLD + "--------------------------------------------------------------------------------" + colores.RESET);
    System.out.println(colores.GREEN_BOLD + "-----            " + colores.RESET + colores.BLUE_BOLD + "Welcome to the best Boat Dealer in Santa Coloma" + colores.RESET + colores.GREEN_BOLD + "           -----" + colores.RESET);
    System.out.println(colores.GREEN_BOLD + "-----  =====     =====     =====     =====     =====     =====     =====   -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  1 - Mostrar catalogo                                                -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  2 - Añadir un barco                                                 -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  3 - Eliminar barco                                                  -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  4 - Guardar                                                         -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  5 - Salir y guardar                                                 -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  6 - Salir sin guardar                                               -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  7 - Volver a atras                                                  -----" + colores.RESET);
    System.out.println(colores.GREEN_BOLD + "-----  =====     =====     =====     =====     =====     =====     =====   -----" + colores.RESET);

  }

  public void mainMenuCompra() {
    System.out.println(colores.CYAN_BOLD + "--------------------------------------------------------------------------------" + colores.RESET);
    System.out.println(colores.CYAN_BOLD + "-----           " + colores.RESET + colores.YELLOW_BOLD + "Welcome to the best Boat Dealer in Santa Coloma" + colores.RESET + colores.CYAN_BOLD+ "            -----" + colores.RESET);
    System.out.println(colores.CYAN_BOLD + "-----  =====     =====     =====     =====     =====     =====     =====   -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  1 - Mostrar catalogo                                                -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  2 - Comprar un barco                                                -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  3 - Mostrar barcos comprados                                        -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  4 - Guardar catalogo                                                -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  5 - Guardar y salir                                                 -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  6 - Salir sin guardar                                               -----" + colores.RESET);
    System.out.println(colores.YELLOW_BOLD + "-----  7 - Volver a atras                                                  -----" + colores.RESET);
    System.out.println(colores.CYAN_BOLD + "-----  =====     =====     =====     =====     =====     =====     =====   -----" + colores.RESET);
  }

  public void menuPrincipal() {
    System.out.println(colores.PURPLE_BOLD + "--------------------------------------------------------------------------------" + colores.RESET);
    System.out.println(colores.PURPLE_BOLD + "-----            " + colores.RESET + colores.BLUE_BOLD + "Welcome to the best Boat Dealer in Santa Coloma" + colores.RESET + colores.PURPLE_BOLD + "           -----" + colores.RESET);
    System.out.println(colores.PURPLE_BOLD + "-----  =====     =====     =====     =====     =====     =====     =====   -----" + colores.RESET);
    System.out.println(colores.BLUE_BOLD + "-----                                                                      -----" + colores.RESET);
    System.out.println(colores.BLUE_BOLD + "-----  1 - Menu de venta                                                   -----" + colores.RESET);
    System.out.println(colores.BLUE_BOLD + "-----  2 - Menu de compra                                                  -----" + colores.RESET);
    System.out.println(colores.BLUE_BOLD + "-----                                                                      -----" + colores.RESET);
    System.out.println(colores.PURPLE_BOLD + "-----  =====     =====     =====     =====     =====     =====     =====   -----" + colores.RESET);
  }

  public int selectMenuPrincipal() {
    System.out.println(colores.WHITE_BOLD + "Escribe una de las opciones" + colores.RESET);
    int opcion1;
    String soption1;
    try {
      soption1 = input.nextLine();
      opcion1 = Integer.parseInt(soption1);
    }
      catch (Exception e){
      sleep(2);
      clearScreen();
      menuPrincipal();
      return selectMenuPrincipal();
      //vuelve a mostrar el menu y opciones
    }
    return opcion1;
  }



  public int selectMainMenuVenta() {
    System.out.println(colores.WHITE_BOLD + "Escribe una de las opciones" + colores.RESET);
    int opcion;
    String soption;
    try {
      soption = input.nextLine();
      opcion = Integer.parseInt(soption);
    }
      catch (Exception e){
      sleep(2);
      clearScreen();
      mainMenuVenta();
      return selectMainMenuVenta();
      //vuelve a mostrar el menu y opciones
    }
    return opcion;
  }

  public int selectMainMenuCompra() {
    System.out.println(colores.WHITE_BOLD + "Escribe una de las opciones" + colores.RESET);
    int opcion2;
    String soption2;
    try {
      soption2 = input.nextLine();
      opcion2 = Integer.parseInt(soption2);
    }
      catch (Exception e){
      sleep(2);
      clearScreen();
      mainMenuCompra();
      return selectMainMenuCompra();
      //vuelve a mostrar el menu y opciones
    }
    return opcion2;
  }

  void clearScreen(){
    System.out.print("\033\143");
  }

  //congelar
  public void sleep(int seconds){
    try{
      TimeUnit.SECONDS.sleep(seconds);
    } catch(Exception e){

    }
  }



}
