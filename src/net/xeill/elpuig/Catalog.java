package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Catalog
*
* @author ASIX1
*
*/

public class Catalog {
  Scanner input = new Scanner(System.in);
  Colores colores = new Colores();

  ArrayList<Boat> barcos = new ArrayList<Boat>();
  ArrayList<Boat> barcosComprados = new ArrayList<Boat>();



  public void showCatalogList(){
    int i = 1;

    for(Boat c : barcos){
        System.out.println(colores.WHITE_BOLD + i + "." + c + colores.RESET);
        i++;
    }
  }

  public void showBuyCatalogList(){
    int i = 1;

    for(Boat c : barcosComprados){
        System.out.println(colores.WHITE_BOLD + i + "." + c + colores.RESET);
        i++;
    }
  }



  public void addNewBoat(){
    System.out.println(colores.GREEN_BOLD + "Escribe las siguientes especificaciones: " + colores.RESET);
    System.out.println(colores.WHITE_BOLD + "Escribe la matrícula del barco: " + colores.RESET);
    String id = input.nextLine();

    System.out.println(colores.RED_BOLD + "Escribe la marca del barco: " + colores.RESET);
    String brand = input.nextLine();

    System.out.println(colores.RED_BOLD + "Escribe el modelo del barco: " + colores.RESET);
    String model = input.nextLine();

    System.out.println(colores.RED_BOLD + "Escribe el precio del barco: " + colores.RESET);
    float price = input.nextFloat();

    System.out.println(colores.RED_BOLD + "Escribe los metros cuadrados del barco: " + colores.RESET);
    int m = input.nextInt();

    System.out.println(colores.RED_BOLD + "Escribe el peso del barco: " + colores.RESET);
    float weight = input.nextFloat();


    Boat boat = new Boat(id, brand, model, price, m, weight);
    barcos.add(boat);
  }

  //Pasa el barco en BufferedReader
  public void addBoatVenta(Boat boat){
    barcos.add(boat);
  }

  //Pasa el barco en BufferedReader
  public void addBoatCompra(Boat boat){
    barcosComprados.add(boat);
  }

  public void removeBoat(){
    showCatalogList();
    System.out.println(colores.BLUE_BOLD + "Selecciona el barco que desea eliminar: " + colores.RESET);
    int opcionEliminar = input.nextInt();

    //Resta 1 para estar igualado a la posicion del array list que empieza por 0
    barcos.remove(opcionEliminar - 1);
  }


  public void buyBoat(){
    showCatalogList();
    System.out.println(colores.PURPLE_BOLD + "Selecciona el barco que desea comprar: " + colores.RESET);
    int opcion = input.nextInt();

    //Restamos 1 al input del usuario para que sea acorde a la posicion del array
    int opcionEliminar = opcion - 1;

    //Guarda el barco que selecciona el usuario
    Boat element = barcos.get(opcionEliminar);

    //Eliminar elemento del array barcos
    barcos.remove(element);

    //Lo añade al array del comprador
    barcosComprados.add(element);


  }

  //Retorna los barcos al buffer writer
  public ArrayList<Boat> getBoats(){
    return barcos;
  }

  //Retorna los barcos al buffer writer
  public ArrayList<Boat> getBoatsComprados(){
    return barcosComprados;
  }



}
