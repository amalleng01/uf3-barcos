package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the FileManager
*
* @author ASIX1
*
*/

public class FileManager {


  //escribe los barcos en el fichero
  public void escribirBarcosVenta(File file, ArrayList<Boat> barcos){
    try {

      //Conecta con el fichero y te permite escribir en el
      BufferedWriter outputStream = new BufferedWriter (new FileWriter(file, false));


      //Escribe todos los barcos
      for (Boat c : barcos) {
        outputStream.write(c.id+":"+c.brand+":"+c.model+":"+c.price+":"+c.m+":"+c.weight+"\n");
      }

      outputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }

  public void escribirBarcosCompra(File file, ArrayList<Boat> barcosComprados){
    try {

      //Conecta con el fichero y te permite escribir en el
      BufferedWriter outputStream = new BufferedWriter (new FileWriter(file, false));


      //Escribe todos los barcos
      for (Boat x : barcosComprados) {
        outputStream.write(x.id+":"+x.brand+":"+x.model+":"+x.price+":"+x.m+":"+x.weight+"\n");
      }

      outputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }

  //lee los barcos del fichero y los convierte en elementos de barco
  public void leerBarcosVenta(File file, Catalog catalog){
    try{
      BufferedReader inputStream = new BufferedReader (new FileReader(file));

      String line = "";


      while ((line = inputStream.readLine()) != null) {
        //separa cuando encuentra dos puntos
        String[] parts = line.split(":");

        Boat c = new Boat(parts[0], parts[1], parts[2], Float.parseFloat(parts[3]), Integer.parseInt(parts[4]),Float.parseFloat(parts[5]));


        //Manda los elementos del array al arraylist de barco
        catalog.addBoatVenta(c);
      }
      inputStream.close();

    }catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }

  //lee los barcos del fichero y los convierte en elementos de barco
  public void leerBarcosCompra(File file, Catalog catalog){
    try{
      BufferedReader inputStream = new BufferedReader (new FileReader(file));

      String line = "";


      while ((line = inputStream.readLine()) != null) {
        //separa cuando encuentra dos puntos
        String[] parts = line.split(":");

        Boat x = new Boat(parts[0], parts[1], parts[2], Float.parseFloat(parts[3]), Integer.parseInt(parts[4]),Float.parseFloat(parts[5]));


        //Manda los elementos del array al arraylist de barco
        catalog.addBoatCompra(x);
      }
      inputStream.close();

    }catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }

}
