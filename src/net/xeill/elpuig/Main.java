package net.xeill.elpuig;

import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  ASIX1
*/
public class Main {
  public static void main(String[] args) {
    run();
  }

  public static void run() {
    Scanner input = new Scanner(System.in);

    Messages m = new Messages();
    Catalog catalog = new Catalog();
    FileManager fm = new FileManager();
    Colores colores = new Colores();

    //Inicia el programa leyendo los barcos en el fichero
    fm.leerBarcosVenta(new File("net/xeill/elpuig/barcosVenta.data"), catalog);
    fm.leerBarcosCompra(new File("net/xeill/elpuig/barcosCompra.data"), catalog);

    boolean salir = false;
    int opcionVenta = 0; //guardamos la opcion del seleccion_usuario
    int opcionPrincipal = 0;
    int opcionCompra = 0;

    boolean showBuy  = false;
    boolean showSell = false;

    while (!salir) {
      //System.out.println("showBuy: " + showBuy + "showSell: " + showSell);


      //comprueba si ningun menu esta en true para volver al menu principal
      if (!showBuy && !showSell){

        m.clearScreen();
        m.menuPrincipal();

        opcionPrincipal = m.selectMenuPrincipal();
        //System.out.println(opcionPrincipal + "Menu principal");

        if (opcionPrincipal == 1){
          showSell = true;
        } if (opcionPrincipal == 2){
          showBuy = true;
        }

      }



      switch (opcionPrincipal) {

        case 1:
        m.mainMenuVenta();
        //System.out.println(opcionVenta + "Menu Venta");


        opcionVenta = m.selectMainMenuVenta();

        switch (opcionVenta) {
          case 1:
              m.clearScreen();
              catalog.showCatalogList();

              break;

          case 2:
            m.clearScreen();
            catalog.addNewBoat();

            break;

          case 3:
            m.clearScreen();
            catalog.removeBoat();


            break;


          case 4:
            //Crea el archivo y le pasa los barcos
            fm.escribirBarcosVenta(new File("net/xeill/elpuig/barcosVenta.data"), catalog.getBoats());
            m.clearScreen();


            break;

          case 5:
            fm.escribirBarcosVenta(new File("net/xeill/elpuig/barcosVenta.data"), catalog.getBoats());
            m.clearScreen();

            //Salir
            salir = true;
            break;


          case 6:
            //salir
            m.clearScreen();

            salir = true;
            break;

          case 7:

            showSell = false;
            break;
          }
          break;

            case 2:
            m.mainMenuCompra();

            opcionCompra = m.selectMainMenuCompra();

            switch (opcionCompra) {
              case 1:
              m.clearScreen();
              catalog.showCatalogList();
              break;


              case 2:
              m.clearScreen();
              catalog.buyBoat();
              break;

              case 3:
              m.clearScreen();
              catalog.showBuyCatalogList();
              break;


              case 4:
              fm.escribirBarcosCompra(new File("net/xeill/elpuig/barcosCompra.data"), catalog.getBoatsComprados());
              m.clearScreen();
              break;

              case 5:
              fm.escribirBarcosCompra(new File("net/xeill/elpuig/barcosCompra.data"), catalog.getBoatsComprados());
              m.clearScreen();

              //Salir
              salir = true;
              break;

              case 6:
              //salir
              m.clearScreen();

              salir = true;
              break;

              case 7:

              showBuy = false;
              break;
          }






      }

    }

  }







}
