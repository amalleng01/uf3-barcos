package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Car
*
* @author ASIX1
*
*/


//Constructor del barco
public class Boat {


//Parametros del barco
  String id;
  String brand;
  String model;
  float price;
  int m; //metros cuadrados
  float weight;

  Boat(String id, String brand, String model, float price, int m, float weight) {
    this.id = id;
    this.brand = brand;
    this.model = model;
    this.price = price;
    this.m = m;
    this.weight = weight;
  }

//Pasa a string todo y lo imprime correctamente como queremos
  public String toString(){
    return id + " " + brand + " " + model + " " + price + " " + m + " " + weight;
  }

}
